import * as yup from 'yup';

let schema = yup.object().shape({
  name: yup.string().required("campo name é obrigatório"),
  email: yup.string().required("campo email é obrigatório").email("Formato inválido"),
  password: yup.string().required("campo password é obrigatório").min(3),
});

export default schema;