import React from 'react';
import { useSelector } from 'react-redux';
import './Sobre.css';

const Sobre = ()=>{
    const user = useSelector((state)=> state.user.user)
    return(
        <div className="containerSobre">
            <span>Sobre</span>
            <p>{user.name}</p>
            
        </div>
    )
}



export default Sobre; 