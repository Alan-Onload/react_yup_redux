import React, { useState } from "react";
import schema from "../../validations/validations";
import "./Cadastro.css";
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux'
import { addUser } from "../../slices/userSlice";
import {yupResolver} from '@hookform/resolvers/yup'

const Cadastro = () => {

  const userRedux = useSelector(state => state.user.user)
  const dispatch = useDispatch();
  const { register, handleSubmit ,formState:{errors} } = useForm({
    resolver: yupResolver(schema),
  });

  const logar = (data) => {
    dispatch(addUser(data))
  };

  return (
    <div className="containerCadastro">
      <p className="">Login</p>
      <form onSubmit={handleSubmit(logar)}>
        <label htmlFor="name">Name</label>
        <div className="input-field">
          <i className="material-icons">person</i>
          <input
            type="text"
            name="name"
            id="name"
            {...register("name")}
          />
        </div>
        <span className="erro">{errors.name?.message}</span>

        <label htmlFor="email">Email</label>
        <div className="input-field">
          <i className="material-icons">email</i>
          <input
            type="email"
            email="email"
            id="email"
            {...register("email")}
          />
        </div>
        <span className="erro">{errors.email?.message}</span>

        <label htmlFor="password">Password</label>
        <div className="input-field">
          <i className="material-icons">password</i>
          <input
            type="password"
            name="password"
            id="password"
            {...register("password")}
          />
        </div>
        <span className="erro">{errors.password?.message}</span>

        <div className="btn-field">
          <button type="submit" className="btn">
            LOGAR
            <i className="material-icons">send</i>
          </button>
        </div>
      </form>
    </div>
  );
};

export default Cadastro;
